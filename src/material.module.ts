import {NgModule} from '@angular/core';

import { MatSliderModule } from '@angular/material/slider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';


@NgModule ({
  imports: [
    MatSliderModule,
    MatSidenavModule,
    MatListModule
  ],
  exports: [
    MatSliderModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule
  ]
})
export class MaterialModule {}
